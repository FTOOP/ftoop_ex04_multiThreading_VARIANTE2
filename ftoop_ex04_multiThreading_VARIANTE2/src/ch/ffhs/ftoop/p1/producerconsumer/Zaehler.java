package ch.ffhs.ftoop.p1.producerconsumer;


public class Zaehler extends Thread {

	private Speicher speicher;
	private int max, min, waitMs;

	/**
	 * Constructor
	 * @param s
	 *            Das Speicherobject, das die aktuelle Zahl haelt.
	 * @param min
	 *            Der Startwert f�r den Zaehler
	 * @param max
	 *            Der Endwert f�r den Zaehler (einschliesslich)
	 * @param waitMs
	 *            Anzahl Millisekunden, die zwischen dem erhoehen der Zahl in s gewartet werden soll.
	 * 
	 */
	Zaehler(Speicher s, int min, int max, int waitMs) {
		this.speicher = s;
		this.max = max;
		this.min = min;
		this.waitMs = waitMs;
	}
	
	/**
	 * Get methode, damit die Drucker Klasse den Wert auslesen kann.
	 * Ist nicht wirklich noetig, aber sauberer, damit man den waitMs-Wert nur einmal hart codieren muss.
	 * @return
	 * 	       int waitMS 
	 */
	public int getWaitMs(){
		return this.waitMs;
	}

	/**
	 * Diese Run Methode z�hlt den Wert in Speicher hoch - von min bis max (einschliesslich).
	 * Detail:
	 * Geloest ueber eine while Schleife mit jeweils dem Warteintervall <waitMs> zwischen jedem Schleifendurchlauf.
	 * Das Warteintervall wird ausgel�st durch die sleep() Methode, die von der Klasse Thread geerbt wurde.
	 */
	@Override
	public void run() { 
		int counter = min;
		while(counter <= max){
			try {
				speicher.setWert(counter);
				counter++;
				sleep(waitMs);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}

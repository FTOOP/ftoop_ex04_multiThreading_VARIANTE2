package ch.ffhs.ftoop.p1.producerconsumer;

import org.junit.Test;

import student.TestCase;

public class ZaehlerDruckerTest extends TestCase {

	public void testZaehlerDrucker() throws InterruptedException {
		ZaehlerDrucker.main(new String[] { "1", "25" });
		assertFuzzyEquals(
				"1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 ",
				systemOut().getHistory());
	}
	

	// Eigener Test
	public void testZaehlerDrucker2() throws InterruptedException {
		ZaehlerDrucker.main(new String[] { "1", "4" });
		String hist = systemOut().getHistory();
		String[] histArr = hist.split(" ");
		
		boolean errorFlag = false;
		if(histArr.length<4) {
			errorFlag = true;
		} else {
			for(int i=0; i<4; i++){
				if(!histArr[i].equals(i+1+"")) errorFlag = true;
			}
		}
		assertFalse(errorFlag);
	}
}

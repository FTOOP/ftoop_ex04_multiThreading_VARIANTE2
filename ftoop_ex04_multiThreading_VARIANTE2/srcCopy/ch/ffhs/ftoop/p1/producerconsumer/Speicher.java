package ch.ffhs.ftoop.p1.producerconsumer;

public class Speicher implements SpeicherIf {

	private int wert;
	private boolean hatWert = false;
	
	/**
	 * Constructor
	 * @param wert Anfangswert
	 */
	public Speicher(int wert) {
		this.wert = wert;
		this.hatWert = true;
	}
	/**
	 * Constructor
	 * Ohne Parameter. wert = 0.
	 */
	public Speicher() {
		this.wert = 0;
		this.hatWert = true;
	}
	
	/**
	 * Gibt den aktuellen Wert zur�ck.
	 * 
	 * @return
	 * @throws InterruptedException
	 */
	public int getWert() throws InterruptedException {
		return wert;
	}
	
	/**
	 * Setzt einen neuen aktuellen Wert.
	 * 
	 * @param wert
	 * @throws InterruptedException
	 */
	public void setWert(int wert) throws InterruptedException {
		this.wert = wert;
	}
	
	
	/**
	 * Gibt true zur�ck, wenn es einen neuen, noch nicht konsumierten Wert im
	 * Objekt hat.
	 * 
	 * @return
	 */
	public boolean isHatWert() {
		// TODO was bedeutet, dass es einen "nicht konsumierten Wert" hat?
		return hatWert;
	}

}
